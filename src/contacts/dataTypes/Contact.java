package contacts.dataTypes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

public class Contact {

	public final int ID;
	private ArrayList<Data> data = new ArrayList<Data>();
	JList<String> jlist = new JList<String>();
	
	public Contact(int id, String fn, String ln, String ph){
		ID = id;//ensures that if two people have the same name they can be seperated.
		
		data.add(new Data("First Name", fn));
		data.add(new Data("Last Name", ln));
		data.add(new Data("Phone Number", ph));
	}
	
	public String toString(){
		String fn = "";
		String ln = "";
		
		for (Data d: data){
			if (d.getName().equals("First Name")) fn = d.getVal();
			if (d.getName().equals("Last Name")) ln = d.getVal();
		}
		
		return(ln.toUpperCase() + ", " + fn);
		
	}

	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param name of data
	 * @return data with the name matching the input string.
	 */
	public Data get(String name){
		for (Data d: data){
			if (name.equals(d.getName())) return d;
		}
		return null;
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @return complete list of data in the contact
	 */
	public ArrayList<Data> get(){
		return data;
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param name of data to be changed
	 * @param new String value for the data
	 */
	public void set(String name, String val){
		for (Data d: data){
			if (name.equals(d.getName())){
				d.set(name, val);
			}
		}
	}
	
	//ensures that the "name" value of each data inserted is unique.
	/**
	 * inserts a new Data object into the list of data objects, ensuring that
	 *  the objects 'name' value is unique (case sensitive).
	 * 
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param name of value to be insterted
	 * @param value to be insterted
	 * @return boolean, true if successfully inserted.
	 */
	public boolean insert(String name, String val){
		for (Data d: data){
			if (d.getName().toLowerCase().equals(name.toLowerCase())){
				return false;
			}
		}
		data.add(new Data(name, val));
		return true;
	}
	
	public void show(JFrame p){
		@SuppressWarnings("unused")
		ShowData sd = new ShowData(p);
	}
	
	class ShowData extends JDialog implements ActionListener {
		private static final long serialVersionUID = 1L;
		
		private static final String FINISH = "FINISH";
		JLabel fnamefield, lnamefield, infofield, datanamefield, phfield;
		
		ArrayList<Data> temp = new ArrayList<Data>();
		
		public ShowData(JFrame p){
			super (p, "View", true);
			
			//Remove all of the data that is displayed seperately.
			for (Data d: data){
				if (d.getName().equals("First Name") || d.getName().equals("Last Name")
						|| d.getName().equals("Phone Number")){
			
				}else{
					temp.add(d);
				}
			}
			
			makeWindow();
			
			setVisible(true);
		}
		
		
		public void makeWindow(){
			getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));			

			JPanel left = new JPanel();
			JPanel right = new JPanel();

			left.setLayout(new BoxLayout(left,BoxLayout.Y_AXIS));
			right.setLayout(new BoxLayout(right,BoxLayout.Y_AXIS));
			
			JLabel label = new JLabel("First Name:");
			label.setFont(new Font("title", 1, 15));
			left.add(label);
			
			fnamefield = new JLabel(get("First Name").getVal());
			left.add(fnamefield);
			
			label = new JLabel("Last Name:");
			label.setFont(new Font("title", 1, 15));
			left.add(label);

			lnamefield = new JLabel(get("Last Name").getVal());
			left.add(lnamefield);
			
			label = new JLabel("Phone Number:");
			label.setFont(new Font("title", 1, 15));
			left.add(label);

			phfield = new JLabel(get("Phone Number").getVal());
			left.add(phfield);
			
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//			RIGHT FIELDS
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			
			label = new JLabel("Additional Data:");
			label.setFont(new Font("title", 1, 15));
			right.add(label);
			
			JPanel buttons = new JPanel();
			buttons.setLayout(new BoxLayout(buttons,BoxLayout.X_AXIS));
			
			right.add(buttons);
			
			jlist.setModel(new AbstractListModel<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getElementAt(int i) {
					return temp.get(i).toString();
				}
			
				@Override
				public int getSize() {
					return temp.size();
				}
			});
		
			jlist.setCellRenderer(new ListCellRenderer<String>(){
				@Override
				public Component getListCellRendererComponent(
					JList<? extends String> list, String value, int index,
					boolean selected, boolean hasfocus) {
						JLabel jl = new JLabel(value);
						jl.setBackground(selected?Color.gray:Color.white);
						jl.setOpaque(true);
						return jl;
					}
			});
		
			jlist.setFixedCellWidth(200);
		
			right.add(jlist);
			right.add(new JScrollPane(jlist));			

			JButton button = new JButton("Done");
			button.addActionListener(this);
			button.setActionCommand(FINISH);
			left.add(button);
			
			getContentPane().add(left, BorderLayout.WEST);
			getContentPane().add(right, BorderLayout.EAST);

			setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			pack();	
			
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand() == FINISH){
				setVisible(false);
			}
		}
	}
	
}
