package contacts.dataTypes;

import java.util.Date;

public class TodoItem {
	public String text;
	public String start, done;
	
	public TodoItem(String text) {
		this.text = text;
		start = "";
		done = "";
	}
	
	/**
	 * changes to the start or end date to the input string based on the 
	 * 	boolean input, true for done date, false for start date.
	 * @param complete
	 * @param date
	 */
	public void insertDate(boolean complete, String date){
		if (complete){
			done = date;
		}else{
			start = date;
		}
	}	
	
	/**
	 * Changes the 'done' string to match today's date signifying completion
	 * 	of the task.
	 */
	public void finish(){
		done = new Date().toString();
	}
	
	
	/**
	 * Returns a string version of the data in the todoItem with the 
	 *  done date if the item has been completed.
	 */
	public String toString(){
		if (done.equals("")){
			return text + " [ Started: " + start + " ]";
		}else{
			return text + " [ Completed: " + done + " ]";
		}
	}
	


}
