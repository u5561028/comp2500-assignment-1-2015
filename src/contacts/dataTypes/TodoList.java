package contacts.dataTypes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import contacts.gui.makeGUI;
import contacts.io.ReadWrite;

public class TodoList {
	public ArrayList<TodoItem> list;

	TodoGUI td;
	
	public TodoList() {
		list = new ArrayList<TodoItem>();
		td = new TodoGUI();
	}

	public void add(TodoItem item){
		list.add(item);
	}
	
	public void remove(int index){
		list.remove(index);
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * Creates a popup window to show the TodoList
	 */
	public void show(){
		td.setVisible(true);
		list = ReadWrite.loadTodo();
		td.jlist.updateUI();
	}
	
	class TodoGUI extends JDialog implements ActionListener {
		private static final long serialVersionUID = 1L;
		
		private static final String FINISH = "FINISH";
		private static final String ADD = "ADD";
		private static final String	COMPLETE = "COMPLETE";
		private static final String	REMOVE = "REMOVE";
		
		JList<String> jlist = new JList<String>();
		
		JTextField infield;

		public TodoGUI() {
			makeGui();
		}
		
		private void makeGui(){
			getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
			
			JPanel top = new JPanel();
			JPanel bottom = new JPanel();
			top.setLayout(new BoxLayout(top,BoxLayout.Y_AXIS));
			bottom.setLayout(new BoxLayout(bottom,BoxLayout.X_AXIS));
			

//****************************************************************
// 							JLIST
//****************************************************************
			
			
			jlist.setModel(new AbstractListModel<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getElementAt(int i) {
					return list.get(i).toString();
				}
			
				@Override
				public int getSize() {
					return list.size();
				}
			});
		
			//changes the colour of the cell when clicked on, indicating the currently selected cell
			jlist.setCellRenderer(new ListCellRenderer<String>(){
				@Override
				public Component getListCellRendererComponent(
					JList<? extends String> list, String value, int index,
					boolean selected, boolean hasfocus) {
						JLabel jl = new JLabel(value);
						jl.setBackground(selected?Color.gray:Color.white);
						jl.setOpaque(true);
						return jl;
					}
			});
			
			jlist.setFixedCellWidth(200);
			
			top.add(jlist);
			top.add(new JScrollPane(jlist));
			

			//****************************************************************
			//					TEXTFIELD AND BUTTONS
			//****************************************************************
			
			JLabel label = new JLabel("Add Item:");
			bottom.add(label);
			
			infield = new JTextField(20);
			bottom.add(infield);
			
			JButton button = makeGUI.addButton(this, "Add", ADD);
			bottom.add(button);
			
			button = makeGUI.addButton(this, "Complete", COMPLETE);
			bottom.add(button);
			
			button = makeGUI.addButton(this, "Remove", REMOVE);
			bottom.add(button);
			
			button = makeGUI.addButton(this, "Exit", FINISH);
			bottom.add(button);
			
			getContentPane().add(top, BorderLayout.NORTH);
			getContentPane().add(bottom, BorderLayout.SOUTH);

			setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			pack();
			
			
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand() == ADD){
				if (infield.getText().contains("=")){
					JOptionPane.showMessageDialog(this, 
							"Please do not use '=' in your todo item");
				}else{
					TodoItem temp = new TodoItem(infield.getText());
					temp.insertDate(false, new Date().toString());
					list.add(temp);
					jlist.updateUI();
				}
				
			}if (ae.getActionCommand() == COMPLETE){
				list.get(jlist.getSelectedIndex()).finish();
				jlist.updateUI();
				
			}if (ae.getActionCommand() == REMOVE){
				list.remove(jlist.getSelectedIndex());
				jlist.updateUI();
				
			}if (ae.getActionCommand() == FINISH){
				ReadWrite.save(list);
				setVisible(false);
			}
			
		}
		
	}

}
