package contacts.dataTypes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import contacts.gui.makeGUI;

public class ContactList {
	public final String title;
	public ArrayList<Contact> contacts;
	private ArrayList<Data> temp = new ArrayList<Data>();
	
	private int IDCount = 0;
	JList<String> jlist = new JList<String>();
	
	

	private static final String FINISH = "FINISH";
	private static final String ADDDATA = "ADDDATA";
	private static final String REMOVEDATA = "REMOVEDATA";
	
	public ContactList(String t){
		title = t;
		contacts = new ArrayList<Contact>();
		
	}
	
	public void startIDCount(int count){
		IDCount = count;
	}
	
	
	public void addContact(JFrame p){
		MakeData md = new MakeData(p);
		md.MakeContact();
	}
	
	public void editContact(JFrame p, int index){
		MakeData md = new MakeData(p, contacts.get(index));  
		md.MakeContact();
	}
	
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param String to search for matches with
	 * @return List of all Contacts with the input string in their firs or last names.
	 */
	public ArrayList<Contact> search(String searchstr){
		ArrayList<Contact> matches = new ArrayList<Contact>();
		
		searchstr = searchstr.toLowerCase();
		
		for (Contact c: contacts){
			if (c.get("First Name").getVal().toLowerCase().contains(searchstr) || 
					c.get("Last Name").getVal().toLowerCase().contains(searchstr)){
				matches.add(c);
			}
		}
		return matches;
	}
	
	public void merge(JFrame p, Contact c){
		SelectMerge sm = new SelectMerge(p, c);
		sm.setVisible(true);
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param contact to be added to list.
	 * @return boolean, true if successfully added
	 */
	private Boolean putContactIntoList(Contact contact){
		
		for (int i = 0; i < contacts.size(); i++){
			//for editing contacts, if the contact already exists, replace it with new version
			if (contacts.get(i).ID == contact.ID){
				contacts.set(i, contact);
				return true;
			}
		}
		
		contacts.add(contact);
		
		for (Contact c: contacts){
			if (c.ID == IDCount) System.out.println("Added: " + c.toString());
		}
		IDCount++;
		return true;
	}
	
	
	public void removeContact(int index){
		if (index < contacts.size()){
			contacts.remove(index);
		}
	}
	
	

	class MakeData extends JDialog implements ActionListener {
		private static final long serialVersionUID = 1L;

		JTextField fnamefield, lnamefield, infofield, datanamefield, phfield;
		JLabel id;
		
		/**
		 * Constructor for MakeData which sets up the JDialogue window for editing
		 * 	the contact rather than making a new contact.
		 * @author Sebastian Van Den Dungen
		 * @param JFrame p
		 * @param Contact to be edited.
		 */
		public MakeData(JFrame p, Contact c){
			super (p, "Edit", true);
			makeWindow();

			id.setText(Integer.toString(c.ID));
			
			for (Data d: c.get()){
				if (d.getName().equals("First Name")){
					fnamefield.setText(d.getVal());
				} else if (d.getName().equals("Last Name")){
					lnamefield.setText(d.getVal());
				} else if (d.getName().equals("Phone Number")){
					phfield.setText(d.getVal());
					
				}else{				
					temp.add(d);
					jlist.updateUI();
				}
			}
			
		}
		
		
		/**
		 * Constructor for JDialogue window to make a new contact
		 * @author Sebastian Van Den Dungen
		 * @param p
		 */
		public MakeData(JFrame p){
			super (p, "New", true);
			makeWindow();
			
		}
		
		public void makeWindow(){
			getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));			

			JPanel left = new JPanel();
			JPanel right = new JPanel();

			left.setLayout(new BoxLayout(left,BoxLayout.Y_AXIS));
			right.setLayout(new BoxLayout(right,BoxLayout.Y_AXIS));
			
			id = new JLabel(Integer.toString(IDCount));
			

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//						RIGHT FIELDS
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			
			JLabel label = new JLabel("First Name:");
			left.add(label);
			
			fnamefield = new JTextField(10);
			left.add(fnamefield);
			
			label = new JLabel("Last Name:");
			left.add(label);

			lnamefield = new JTextField(10);
			left.add(lnamefield);
			
			label = new JLabel("Phone Number:");
			left.add(label);

			phfield = new JTextField(10);
			left.add(phfield);
			
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//			RIGHT FIELDS
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			
			label = new JLabel("Additional Data:");
			right.add(label);
			
			label = new JLabel("Name of Data (e.g. Address):");
			right.add(label);
			
			datanamefield = new JTextField(10);
			right.add(datanamefield);
			
			label = new JLabel("Data (e.g. 14 Walaby Way):");
			right.add(label);
			
			infofield = new JTextField(10);
			right.add(infofield);
			
			JPanel buttons = new JPanel();
			buttons.setLayout(new BoxLayout(buttons,BoxLayout.X_AXIS));

			
			JButton button = makeGUI.addButton(this, "Add", ADDDATA); 
			buttons.add(button);

			button = makeGUI.addButton(this, "Remove", REMOVEDATA);
			buttons.add(button);
			
			right.add(buttons);
			
			jlist.setModel(new AbstractListModel<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getElementAt(int i) {
					return temp.get(i).toString();
				}
			
				@Override
				public int getSize() {
					return temp.size();
				}
			});
		
			//changes the colour of the cell when clicked on, indicating the currently selected cell
			jlist.setCellRenderer(new ListCellRenderer<String>(){
				@Override
				public Component getListCellRendererComponent(
					JList<? extends String> list, String value, int index,
					boolean selected, boolean hasfocus) {
						JLabel jl = new JLabel(value);
						jl.setBackground(selected?Color.gray:Color.white);
						jl.setOpaque(true);
						return jl;
					}
			});
		
			jlist.setFixedCellWidth(200);
		
			right.add(jlist);
			right.add(new JScrollPane(jlist));			

			button = makeGUI.addButton(this, "Done", FINISH);
			left.add(button);
			
			getContentPane().add(left, BorderLayout.WEST);
			getContentPane().add(right, BorderLayout.EAST);

			setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			pack();	
			
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			//ArrayList<Data> data = new ArrayList<Data>();
			
			if (ae.getActionCommand() == FINISH){

				String fname = fnamefield.getText();
				String lname = lnamefield.getText();
				String ph = phfield.getText();
				
				//Check for illegal characters before saving, if they exist show a popup warning.
				if (fname.contains(":") || fname.contains(",") ||
						lname.contains(":") || lname.contains(",") ||
						ph.contains(":") || ph.contains(",")){
					JOptionPane.showMessageDialog(this, 
							"Please do not use ':' or ',' in your data names or information");
				}else{
				
					Contact c = new Contact(Integer.parseInt(id.getText()), fnamefield.getText(), 
										lnamefield.getText(), phfield.getText());
					for (Data d: temp){
						c.insert(d.getName(), d.getVal());	
					}
					
					if (putContactIntoList(c)){
						temp = new ArrayList<Data>();
						setVisible(false);
					}
				}
			}if (ae.getActionCommand() == ADDDATA){
				String dname = datanamefield.getText();
				String info = infofield.getText();
				
				//show popup if illegal characters are found in the data.
				if (dname.contains(":") || dname.contains(",") 
						|| info.contains(":") || info.contains(",")){
					JOptionPane.showMessageDialog(this, 
							"Please do not use ':' or ',' in your data names or information");
				}else{
					temp.add(new Data(datanamefield.getText(), infofield.getText()));
					jlist.updateUI();
				}
			}if (ae.getActionCommand() == REMOVEDATA){
				temp.remove(jlist.getSelectedIndex());
				jlist.updateUI();
			}
			
			
		}
		
		public void MakeContact(){
			setVisible(true);
		}
		
		
	}


	class SelectMerge extends JDialog implements ActionListener {
		private static final long serialVersionUID = 1L;
		
		JList<String> jlist = new JList<String>();
		Contact c1, c2;
		
		ArrayList<Contact> rest = new ArrayList<Contact>();
		
		
		public SelectMerge(JFrame p, Contact c){
			super(p, "Merge", true);
			
			c1 = c;
			//new Contact(c.ID, c.get("First Name").getVal(), c.get("Last Name").getVal(), c.get("Phone Number").getVal());
			
			for (Contact con: contacts){
				if (con.ID != c.ID){
					rest.add(con);
				}
			}
			
			makeGUI();
		}
		
		public void makeGUI(){
			JPanel top = new JPanel(), bottom = new JPanel();

			top.setLayout(new BoxLayout(top,BoxLayout.Y_AXIS));
			bottom.setLayout(new BoxLayout(bottom,BoxLayout.Y_AXIS));	
			
			JLabel label = new JLabel("Select contact to merge with:");
			top.add(label);
			
			jlist.setModel(new AbstractListModel<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getElementAt(int i) {
					return rest.get(i).toString();
				}
			
				@Override
				public int getSize() {
					return rest.size();
				}
			});
		
			//changes the colour of the cell when clicked on, indicating the currently selected cell
			jlist.setCellRenderer(new ListCellRenderer<String>(){
				@Override
				public Component getListCellRendererComponent(
					JList<? extends String> list, String value, int index,
					boolean selected, boolean hasfocus) {
						JLabel jl = new JLabel(value);
						jl.setBackground(selected?Color.gray:Color.white);
						jl.setOpaque(true);
						return jl;
					}
			});
			
			jlist.setFixedCellWidth(200);
			
			top.add(jlist);
			top.add(new JScrollPane(jlist));
			
			
			JButton button = makeGUI.addButton(this, "Merge", FINISH);
			bottom.add(button);
			
			getContentPane().add(top, BorderLayout.NORTH);
			getContentPane().add(bottom, BorderLayout.SOUTH);

			setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			pack();	
			
			
			
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand() == FINISH){
				c2 = rest.get(jlist.getSelectedIndex());
				setVisible(false);
				
				//merge contacts, saving differences as alias's
				ArrayList<Data> d = c2.get();
				
			
				for (int i = 0; i < d.size(); i++){
					if (d.get(i).getName().equals("First Name") 
							&& (!d.get(i).getVal().equals(c1.get("First Name"))
							|| !c1.get("Last Name").equals(c2.get("Last Name")))){
							//if the value is the first name, and the contact c2 has
							// either a different first name or a different last name
							// add c2's name as an alias for c1
						
						String a = "Alias ";
						int j = 1;
						
						while (!c1.insert(a, (d.get(i).getVal() + " " + c2.get("Last Name").getVal()))){
							j++;
							a = "Alias (" + j + ")"; 
						}
						
					}else if (!d.get(i).getName().equals("Last Name")){
						//if name is not first or last name
						boolean exists = false;
						for (Data d1: c1.get()){
							if (d.get(i).getName().equals(d1.getName())){//has data with same name
								if (!d.get(i).getVal().equals(d1.getVal())){ // data is different
									exists = true;
								}
							}
						}
						
						if (exists){
							
							int j = 2;
							String add = " (" + j +")";
							while (!c1.insert(d.get(i).getName() + add, d.get(i).getVal())){
								j++;
								add = " (" + j +")";
							}
						}else{
							c1.insert(d.get(i).getName(), d.get(i).getVal());
						}
						
					}
					
				}
				
				
				for (int i = 0; i < contacts.size(); i++){
					if (contacts.get(i).ID == c2.ID){
						contacts.remove(i);
						break;
					}
				}
			}
		}
		
	}
	
}
