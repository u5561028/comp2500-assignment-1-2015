package contacts.dataTypes;

public class Data {
	
	// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	// NOTE: this data type uses 
	//        the name variable as
	//        an unique identifier
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	
	private String name;
	private String value;
	
	public Data (String n, String v){
		name = n;
		value = v;
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param String for the name of the new Data
	 * @param Strang for the value of the new Data
	 */
	public void set(String n, String v){
		n = name;
		v = value;
	}
	
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @return name of data
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @return the value of the Data
	 */
	public String getVal(){
		return value;
	}
	
	
	public String toString(){
		return name + ": " + value;
	}
	
	
}
