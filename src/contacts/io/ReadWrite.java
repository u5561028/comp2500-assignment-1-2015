package contacts.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;

import contacts.dataTypes.*;

public class ReadWrite {
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param list of contacts to be saved
	 */
	public static void save(ContactList list){
		String filedir = "contacts/" + list.title;
		String filename = filedir + "/" + list.title;
		
		//TODO find better implementation of this. 
		
		//creates directory for storing the list.
		@SuppressWarnings("unused")
		boolean f = new File(filedir).mkdirs();
		
		try {
			BufferedWriter lw = Files.newBufferedWriter(
					new File(filename).toPath(), 
					Charset.forName("US-ASCII"));
			for (Contact c: list.contacts){
				for (Data d: c.get()){
					lw.append(d.getName() + ":" + d.getVal() + ",");
				}
				lw.newLine();
			}
			lw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
	
	/**
	 * Saves a copy of the file in another directory over another file.
	 * 
	 * @author Sebastian Van Den Dungen;
	 * 
	 * @param Contact List to be Saved.
	 * @param Location of file to overwrite.
	 */
	public static void saveAS(ContactList list, String path){
		String filename = path;
		try {
			BufferedWriter lw = Files.newBufferedWriter(
					new File(filename).toPath(), 
					Charset.forName("US-ASCII"));
			for (Contact c: list.contacts){
				for (Data d: c.get()){
					lw.append(d.getName() + ":" + d.getVal() + ",");
				}
				lw.newLine();
			}
			lw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * @param path to file to be loaded.
	 * @return constructed Contact List
	 */
	public static ContactList load(String path){		
		try {
			ContactList clist = new ContactList(new File(path).getName());
			
			System.out.println(clist.title);
			
			BufferedReader lr = Files.newBufferedReader(new File(path).toPath(), Charset.forName("US-ASCII"));
			
			String rawContact = "";
			String[] splitContact = null;
			int IDCount = 0;
			
			while ((rawContact = lr.readLine()) != null){
				splitContact = rawContact.split(",");
				
				String[] dataPair;
				String fn = "", ln = "", ph = "";
				
				ArrayList<Data> data = new ArrayList<Data>();
				
				for (String s: splitContact){
					dataPair = s.split(":");

					if (dataPair[0].equals("First Name")) fn = dataPair[1];
					else if (dataPair[0].equals("Last Name")) ln = dataPair[1];
					else if (dataPair[0].equals("Phone Number")) ph = dataPair[1];
					else data.add(new Data(dataPair[0], dataPair[1]));
					
				}
				
				Contact c = new Contact(IDCount, fn, ln, ph);
				IDCount++;
				for (Data d: data){
					c.insert(d.getName(), d.getVal());
				}

				clist.contacts.add(c);
			}			
			
			clist.startIDCount(IDCount);
			
			return clist;
			
		}catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<TodoItem> loadTodo(){
		ArrayList<TodoItem> list = new ArrayList<TodoItem>();
		
		try {
			BufferedReader lr = Files.newBufferedReader(
					new File("todolist").toPath(), Charset.forName("US-ASCII"));
			
			String rawItem = "";
			while ((rawItem = lr.readLine()) != null){
				String[] items = rawItem.split("=");
							
				TodoItem temp = new TodoItem(items[0]);
				boolean complete = Boolean.parseBoolean(items[1]);
				temp.insertDate(complete, items[2]);
				
				list.add(temp);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static void save(ArrayList<TodoItem> list){
		try {
			BufferedWriter lw = Files.newBufferedWriter(
					new File("todolist").toPath(), Charset.forName("US-ASCII"));

			for (TodoItem td: list){
				boolean complete;
				String date;
				
				if  (td.done.equals("")){
					complete = false;
					date = td.start;
				}else{
					complete = true;
					date = td.done;
				}
				
				lw.append(td.text + "=" + complete + "=" + date);
				lw.newLine();
			}
			lw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
