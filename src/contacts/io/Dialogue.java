package contacts.io;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Dialogue extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	JTextField textfield;
	
	public Dialogue(JFrame p){
		super (p, "New", true);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

		JLabel label = new JLabel("Please type a name for");
		getContentPane().add(label);
		label = new JLabel("the new Contact List");
		getContentPane().add(label);
		
		textfield = new JTextField(20);
		getContentPane().add(textfield);
		
		
		JButton button = new JButton("Done");
		button.addActionListener(this);
		getContentPane().add(button);

		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		pack();
	}

	public String getText() {
		setVisible(true);
		return textfield.getText();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		setVisible(false);
	}
	
}
