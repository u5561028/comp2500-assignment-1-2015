package contacts.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import contacts.dataTypes.Contact;
import contacts.dataTypes.ContactList;
import contacts.dataTypes.Data;
import contacts.dataTypes.TodoList;
import contacts.io.Dialogue;
import contacts.io.ReadWrite;

public class Window implements Runnable, ActionListener{

	public static final String EXITCOMMAND = "EXITCOMMAND";
	public static final String NEWCOMMAND = "NEWCOMMAND";
	public static final String LOADCOMMAND = "LOADCOMMAND";
	public static final String SAVECOMMAND = "SAVECOMMAND";
	public static final String SAVEASCOMMAND = "SAVEASCOMMAND";
	public static final String NEWCONTACT = "NEWCONTACT";
	public static final String EDITCONTACT = "EDITCONTACT";
	public static final String VIEWCONTACT = "VIEWCONTACT";
	public static final String REMOVECONTACT = "REMOVECONTACT";
	public static final String SEARCHCOMMAND = "SEARCHCOMMAND";
	public static final String CANCELCOMMAND = "CANCELCOMMAND";
	public static final String MERGECONTACT = "MERGE";
	public static final String VIEWTODO = "VIEWTODO";
	
	private boolean changes = false;
	JFrame frame = new JFrame("Contact Manager");
	JList<String> jlist = new JList<String>();
	
	JLabel listLabel;
	ContactList clist = new ContactList("Default");
	TodoList tlist = new TodoList();
	ArrayList<Contact> selected = clist.contacts;
	
	JTextField searchField;

	public static void main(String[] args) {
		new Window();
	}
		
	public Window(){
		SwingUtilities.invokeLater(this);
	}
	
	public void run(){
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
			
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//   MENU BAR 
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

		JMenuBar menuBar = makeGUI.makeMenu(this);
		
		frame.setJMenuBar(menuBar);
		
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//  CONTACT ACTIONS; ADD, REMOVE, EDIT
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		JPanel contactactions = new JPanel();
     	contactactions.setLayout(new BoxLayout(contactactions,BoxLayout.Y_AXIS));

     	JPanel top = new JPanel();
     	JPanel bottom = new JPanel();
     	
     	top.setLayout(new BoxLayout(top, BoxLayout.X_AXIS));
     	bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
     	     	
     	JButton jbutton = makeGUI.addButton(this, "New Contact", NEWCONTACT);//Add Contact Button
     	top.add(jbutton);
     
     	jbutton = makeGUI.addButton(this, "Remove Contact", REMOVECONTACT);//Remove Contact Button
     	top.add(jbutton);
     	
     	jbutton = makeGUI.addButton(this, "Edit Contact", EDITCONTACT);//Edit Contact Button
     	top.add(jbutton);
     	
     	jbutton = makeGUI.addButton(this, "View Contact", VIEWCONTACT);//View Contact Button
     	bottom.add(jbutton);
     	
     	jbutton = makeGUI.addButton(this, "Merge Contact", MERGECONTACT);
     	bottom.add(jbutton);
     	
     	contactactions.add(top);
     	contactactions.add(bottom);
     	
     	frame.getContentPane().add(contactactions,BorderLayout.PAGE_END);
	
		
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// 	CONTACT LIST
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		JPanel listArea = new JPanel();
     	listArea.setLayout(new BoxLayout(listArea,BoxLayout.Y_AXIS));
     
     	listArea.setBackground(Color.WHITE);
     	listLabel = new JLabel(clist.title, SwingConstants.CENTER);
     	listLabel.setFont(new Font("listtitle", 1,  12));
     	
     	
     	
    	//Contact List
		jlist.setModel(new AbstractListModel<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getElementAt(int i) {
					//return clist.contacts.get(i).toString();
					return selected.get(i).toString();
				}
			
				@Override
				public int getSize() {
					//return clist.contacts.size();
					return selected.size();
				}
			});
		
		jlist.setCellRenderer(new ListCellRenderer<String>(){
			@Override
			public Component getListCellRendererComponent(
					JList<? extends String> list, String value, int index,
					boolean selected, boolean hasfocus) {
				JLabel jl = new JLabel(value);
				jl.setBackground(selected?Color.gray:Color.white);
				jl.setOpaque(true);
				return jl;
			}});
		
		jlist.setFixedCellWidth(200);
		
		listArea.add(listLabel);
		listArea.add(jlist);
		listArea.add(new JScrollPane(jlist));
     	frame.getContentPane().add(listArea, BorderLayout.EAST);
     	
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//     	SEARCH LIST
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
     	JPanel searchBar = new JPanel();
     	
     	searchBar.setLayout(new BoxLayout(searchBar, BoxLayout.X_AXIS));
     	
     	searchField = new JTextField(10);
     	
     	searchField.setText("Search Contacts...");
     	
     	searchField.addFocusListener(new java.awt.event.FocusAdapter() {
     	    public void focusGained(java.awt.event.FocusEvent evt) {
     	       if (searchField.getText().equals("Search Contacts...")) {
     	    	  searchField.setText("");
     	       }
     	    }
     	    
     	    public void focusLost(java.awt.event.FocusEvent evt){
     	    	if (searchField.getText().equals("")){
     	    		searchField.setText("Search Contacts...");
     	    	}
     	    }
     	});
     	searchBar.add(searchField);
     	
     	
		//Cancel Search Button
     	jbutton = makeGUI.addButton(this, "X", CANCELCOMMAND);
     	searchBar.add(jbutton);
		
     	//Search Contact Button
     	jbutton = makeGUI.addButton(this, "Search", SEARCHCOMMAND);
     	searchBar.add(jbutton);

     	frame.getContentPane().add(searchBar, BorderLayout.PAGE_START);
     	
     	//frame.pack();
     	frame.setVisible(true);
	}
	
	private void exit(){
		System.exit(0);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		JFileChooser filechooser = new JFileChooser();
		
		//System.out.println("The " + ae.getActionCommand() + " was selected." );
		if (ae.getActionCommand() == EXITCOMMAND){
			if (!changes) exit();
			else{
				//Returns, 0,1 or 2 depending on option chosen
				int response = JOptionPane.showConfirmDialog(frame,
						"You have Unsaved Changes, \nDo you still wish to save changes before exiting?");
				
				if (response == 0){//yes
					ReadWrite.save(clist);
					exit();
				}else if (response == 1){//no
					exit();
					
				}else{//cancel
					
				}
			}
		}if (ae.getActionCommand() == LOADCOMMAND){
			int res = filechooser.showOpenDialog(frame);
			String path = null;
			if (res == JFileChooser.APPROVE_OPTION) {
				path = filechooser.getSelectedFile().getPath();
				System.out.println("selected file : "
						+ path);
				
			}
			if (path != null){
				clist = ReadWrite.load(path);
				listLabel.setText(clist.title);
				update();
			}
			 
			 
		}if (ae.getActionCommand() == SAVECOMMAND){
			ReadWrite.save(clist);
			listLabel.setText(clist.title);
			changes = false;
			
			
		}if (ae.getActionCommand() == SAVEASCOMMAND){
			
			int res = filechooser.showOpenDialog(frame);
			String path = null;
			if (res == JFileChooser.APPROVE_OPTION) {
				path = filechooser.getSelectedFile().getPath();
				System.out.println("selected file : "
						+ path);
				
			}
			if (path != null) {
				ReadWrite.saveAS(clist, path);
				listLabel.setText(clist.title);
			}
			
			
		}if (ae.getActionCommand() == NEWCOMMAND){
			Dialogue dialog = new Dialogue(frame);

			if (!changes) {
				dialog = new Dialogue(frame);
				String ret = dialog.getText();
				makeNewList(ret);
			}else{
				//Returns, 0,1 or 2 depending on option chosen
				int response = JOptionPane.showConfirmDialog(frame,
						"You have unsaved changes to " + clist.title +"," +
								"\nDo you still wish to save these changes?");
				
				if (response == 0){//yes
					ReadWrite.save(clist);
					dialog = new Dialogue(frame);
					String ret = dialog.getText();
					makeNewList(ret);
					
				}else if (response == 1){//no
					dialog = new Dialogue(frame);
					String ret = dialog.getText();
					makeNewList(ret);
					
				}else{//cancel
					
				}
			}
			update();
			
		}if (ae.getActionCommand() == NEWCONTACT){
			changes = true;
			clist.addContact(frame);
			update();
			listLabel.setText(clist.title + "*");
			
			
		}if (ae.getActionCommand() == REMOVECONTACT){
			if (jlist.getSelectedIndex() >= 0){
				clist.removeContact(jlist.getSelectedIndex());
				update();
				changes = true;
				listLabel.setText(clist.title + "*");
			}
			
		}if (ae.getActionCommand() == EDITCONTACT){
			clist.editContact(frame, jlist.getSelectedIndex());
			update();
			changes = true;
			listLabel.setText(clist.title + "*");
			
		}if (ae.getActionCommand() == VIEWCONTACT){
			clist.contacts.get(jlist.getSelectedIndex()).show(frame);
			
		}if (ae.getActionCommand() == SEARCHCOMMAND){
			selected = clist.search(searchField.getText());
			jlist.updateUI();
			
		}if (ae.getActionCommand() == CANCELCOMMAND){
			update();
			searchField.setText("Search Contacts...");
		}if (ae.getActionCommand() == VIEWTODO){
			tlist.show();
		}if (ae.getActionCommand() == MERGECONTACT){
			clist.merge(frame, clist.contacts.get(jlist.getSelectedIndex()));
			update();
		}
		
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * Creates a new contact list with the title equal to the input string.
	 * @param name
	 */
	private void makeNewList(String name){
		clist = new ContactList(name);
		changes = true;
		listLabel.setText(name + "*");//indicates unsaved changes
	}
	
	private void update(){
		selected = clist.contacts;
		if (!searchField.getText().equals("Search Contacts...")){
			selected = clist.search(searchField.getText());
		}
		jlist.updateUI();
		frame.setTitle("Contact Manager: Contact Count: " + clist.contacts.size());
	}
	
	
	@Test
	public void Loadtests(){
		ContactList testlist1 = new ContactList("testing");
		int cID = 0;
		
		//Generate a large sample of contacts and data to test loading and saving.
		while (cID < 100){
			Contact c1 = new Contact(cID, Double.toString(Math.random())
					, Double.toString(Math.random()), Double.toString(Math.random()));
			
			int dcount = (int) (100*(Math.random()));
			while (dcount > 0){
				dcount--;
				c1.insert("Data"+dcount, Double.toString(Math.random()));
			}
			
			
			testlist1.contacts.add(c1);
			cID++;
		}
		
		ReadWrite.save(testlist1);
		
		ContactList testlist2 = ReadWrite.load("./contacts/testing/testing");

		assertEquals(testlist1.contacts.size(), testlist2.contacts.size());
		
		for (int j = 0; j < testlist1.contacts.size(); j++){
			assertEquals(testlist1.contacts.get(0).ID, testlist2.contacts.get(0).ID);
			
			ArrayList<Data> data = testlist1.contacts.get(0).get();
			ArrayList<Data> data2 = testlist2.contacts.get(0).get();
			
			assertEquals(data.size(), data2.size());
			
			//compare all pieces of data and look for inconsistancies.
			for (int i = 0; i < data.size(); i++){
				assertEquals(data.get(i).getName(), data2.get(i).getName());
				assertEquals(data.get(i).getVal(), data2.get(i).getVal());
			}
		}
		
		
	}
	
	@Test
	public void deletion(){
		ContactList testlist1 = new ContactList("testing");
		int cID = 0;
		
		while (cID < 20){
			Contact c1 = new Contact(cID, Double.toString(Math.random())
					, Double.toString(Math.random()), Double.toString(Math.random()));
			
			testlist1.contacts.add(c1);
			cID++;
		}
		
		ContactList testlist2 = new ContactList("testing2");
		for (Contact c: testlist1.contacts){
			testlist2.contacts.add(c);
		}
		
		int removed = (int) (20*(Math.random()));		
		
		testlist2.removeContact(removed);

		System.out.println(testlist1.contacts.size());
		System.out.println(testlist2.contacts.size());
		
		//test if size of lists is the same.
		assertEquals(testlist1.contacts.size()-1, testlist2.contacts.size());
		
		assertEquals(0, testlist2.search(
				testlist1.contacts.get(removed).get("First Name").getVal()).size());
		
	}
	
	@Test
	public void addition(){
		ContactList testlist1 = new ContactList("testing");
		int cID = 0;
		
		while (cID < 20){
			Contact c1 = new Contact(cID, Double.toString(Math.random())
					, Double.toString(Math.random()), Double.toString(Math.random()));
			
			testlist1.contacts.add(c1);
			cID++;
		}
		
		ContactList testlist2 = new ContactList("testing2");
		for (Contact c: testlist1.contacts){
			testlist2.contacts.add(c);
		}
		
		Contact c = new Contact(cID, "John", "Smith", "19191919");
		c.insert("Email", "john.smith@hotmail.com");
		testlist2.contacts.add(c);
		
		//test if size of lists is the same.
		assertEquals(testlist1.contacts.size()+1, testlist2.contacts.size());
		
		//check if contact actually exists within the list.
		assertEquals(1, testlist2.search("John").size());
	
	}
	
	

}
