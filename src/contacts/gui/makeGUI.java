package contacts.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class makeGUI {
	
	public static JMenuBar makeMenu(Window w){
		JMenuBar menuBar;
		JMenu menu;
		JMenuItem menuItem;
		menuBar = new JMenuBar();
		
		menu = new JMenu("File");
		menu.setMnemonic(KeyEvent.VK_F);//Sets hotkey
		menuBar.add(menu);
		
		//NEW Contact List
		menuItem = makeItem(w, "New", Window.NEWCOMMAND, KeyEvent.VK_N, KeyEvent.VK_N);
		menu.add(menuItem);	
		menu.addSeparator();
				
		//LOAD existing Contact List
		menuItem = makeItem(w, "Open", Window.LOADCOMMAND, KeyEvent.VK_O, KeyEvent.VK_O);
		menu.add(menuItem);
		
		//SAVE current Contact List
		menuItem = makeItem(w, "Save", Window.SAVECOMMAND, KeyEvent.VK_S, KeyEvent.VK_S);
		menu.add(menuItem);
			
		//SAVE current AS Contact List
		menuItem = makeItem(w, "Save As", Window.SAVEASCOMMAND, KeyEvent.VK_A, KeyEvent.VK_A);
		menu.add(menuItem);
		menu.addSeparator();
		
		//EXIT button
		menuItem = makeItem(w, "Exit", Window.EXITCOMMAND, KeyEvent.VK_X, KeyEvent.VK_Q);
		menu.add(menuItem);
		
		
		//###########################
		//	  View Dropdown menu
		//###########################
		
		menu = new JMenu("View");
		menu.setMnemonic(KeyEvent.VK_V);//Sets hotkey
		
		menuItem = makeItem(w, "Todo List", Window.VIEWTODO, KeyEvent.VK_L, KeyEvent.VK_L);
		menu.add(menuItem);
		
		menuItem = makeItem(w, "Contact", Window.VIEWCONTACT, KeyEvent.VK_C, KeyEvent.VK_C);
		menu.add(menuItem);
		
		menuBar.add(menu);
		
		return menuBar;
	}
	
	public static JMenuItem makeItem(Window w, String title, String COMMAND, int altkey, int hotkey){
		JMenuItem menuItem = new JMenuItem(title, altkey);
			menuItem.setAccelerator(KeyStroke.getKeyStroke(hotkey, ActionEvent.CTRL_MASK));
			menuItem.getAccessibleContext().setAccessibleDescription("Exits the program");
					
			//Adds Functionality to the button in the file menu
			menuItem.addActionListener(w);
			menuItem.setActionCommand(COMMAND);
		return menuItem;
	}
	
	public static JButton addButton(ActionListener w, String title, String COMMAND){
     	JButton jbutton = new JButton(title);
     	jbutton.setActionCommand(COMMAND);
     	jbutton.addActionListener(w);
     	return jbutton;
	}
}
